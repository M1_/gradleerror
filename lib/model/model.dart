import 'dart:convert';

DataModelsSurabaya dataModelsSurabayaFromJson(String str) =>
    DataModelsSurabaya.fromJson(json.decode(str));

String dataModelsSurabayaToJson(DataModelsSurabaya data) =>
    json.encode(data.toJson());

class DataModelsSurabaya {
  DataModelsSurabaya({
    this.status,
    this.msg,
    this.data,
  });

  bool status;
  String msg;
  List<Datum> data;

  factory DataModelsSurabaya.fromJson(Map<String, dynamic> json) =>
      DataModelsSurabaya(
        status: json["status"],
        msg: json["msg"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "msg": msg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.className,
    this.kode,
    this.namaGudang,
    this.kota,
  });

  String className;
  String kode;
  String namaGudang;
  String kota;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        className: json["ClassName"],
        kode: json["Kode"],
        namaGudang: json["NamaGudang"],
        kota: json["Kota"],
      );

  Map<String, dynamic> toJson() => {
        "ClassName": className,
        "Kode": kode,
        "NamaGudang": namaGudang,
        "Kota": kota,
      };
}
