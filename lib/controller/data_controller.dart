import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_application_2/model/model.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class DataController extends GetxController {
  // Future<void> getData() async {
  //   var response = await http.get(Uri.parse(
  //       "https://development.semangatdong.com/devpgr/api/getGudang?kota=surabaya"));

  // final listData = Rxn<DataModelsSurabaya>();
  // var isLoading = false.obs;

  final Dio dio = Dio();
  final Connectivity _connectivity = Connectivity();

  Future<DataModelsSurabaya> getData() async {
    ConnectivityResult connectivityResult =
        await _connectivity.checkConnectivity();
    SharedPreferences pref = await SharedPreferences.getInstance();

    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      final response = await dio.get(
          "https://development.semangatdong.com/devpgr/api/getGudang?kota=surabaya");
      var data = jsonDecode(response.toString());
      if (response.statusCode == 200) {
        update();
        pref.setString('gudangData', jsonEncode(data));
        print("data saved: " + pref.getString('gudangData'));
        // Map jsonSaved = jsonDecode(pref.getString('gudangData'));
        // var user = DataModelsSurabaya.fromJson(jsonSaved);

        return DataModelsSurabaya.fromJson(response.data);
      } else {}
    } else {
      Map jsonSaved = jsonDecode(pref.getString('gudangData'));
      var data = DataModelsSurabaya.fromJson(jsonSaved);
      print(data);
      return data;
    }
  }
}
