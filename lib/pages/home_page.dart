import 'package:flutter/material.dart';
import 'package:flutter_application_2/controller/data_controller.dart';
import 'package:flutter_application_2/model/model.dart';
import 'package:get/get.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DataController mycontroller = Get.find<DataController>();

  Future<void> _refreshData(BuildContext context) async {
    return mycontroller.getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("data"),
        centerTitle: true,
      ),
      body: RefreshIndicator(
        onRefresh: () => _refreshData(context),
        child: FutureBuilder<DataModelsSurabaya>(
          future: mycontroller.getData(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text(snapshot.error.toString()),
              );
            } else if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data.data.length,
                  itemBuilder: (context, index) => Card(
                        child: ListTile(
                          onTap: () => Get.toNamed(
                            '/detail',
                            arguments: {
                              'classname': snapshot.data.data[index].className,
                              'kode': snapshot.data.data[index].kode,
                              'kota': snapshot.data.data[index].kota,
                              'namagudang': snapshot.data.data[index].namaGudang
                            },
                          ),
                          title: Text(
                            snapshot.data.data[index].className,
                            overflow: TextOverflow.ellipsis,
                          ),
                          subtitle: Text(snapshot.data.data[index].kode),
                        ),
                      ));
            }
            return const Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
