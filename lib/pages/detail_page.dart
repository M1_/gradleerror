import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("detail"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Text("Classname: " + Get.arguments['classname'] ?? ''),
          Text("kode: " + Get.arguments['kode'] ?? ''),
          Text("kota: " + Get.arguments['kota'] ?? ''),
          Text("nama Gudang: " + Get.arguments['namagudang'] ?? '')
        ],
      ),
    );
  }
}
