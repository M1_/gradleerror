import 'package:flutter_application_2/controller/data_controller.dart';
import 'package:flutter_application_2/pages/detail_page.dart';
import 'package:flutter_application_2/pages/home_page.dart';
import 'package:get/get.dart';

import 'app_routes.dart';

class AppPages {
  static var list = [
    GetPage(
      name: AppRoutes.homepage,
      page: () => const HomePage(),
      binding: BindingsBuilder(() {
        Get.put(DataController());
      }),
    ),
    GetPage(name: AppRoutes.detail, page: () => const DetailPage())
  ];
}
